from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.


@login_required
def list_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipts_list.html", context)


@login_required(login_url="/accounts/login")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required(login_url="/accounts/login")
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.filter(purchaser=request.user)
    count = receipts.count()
    context = {
        "receipts": receipts,
        "categories": categories,
        "count": count,
    }
    return render(request, "receipts/category_list.html", context)


@login_required(login_url="/accounts/login")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("categories")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required(login_url="/accounts/login")
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.filter(purchaser=request.user)
    count = receipts.count()
    context = {
        "receipts": receipts,
        "accounts": accounts,
        "count": count,
    }
    return render(request, "receipts/accounts_list.html", context)


@login_required(login_url="/accounts/login")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("accounts")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
