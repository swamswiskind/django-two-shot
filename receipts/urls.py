from django.urls import path
from receipts.views import (
    list_receipts,
    create_receipt,
    create_category,
    create_account,
    category_list,
    account_list,
)

urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="categories"),
    path("accounts/", account_list, name="accounts"),
]
